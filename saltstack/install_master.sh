#!/usr/bin/bash

###############################################################
#  TITRE: 
#
#  AUTEUR:   Xavier
#  VERSION: 
#  CREATION:  
#  MODIFIE: 
#
#  DESCRIPTION: 
###############################################################



# Variables ###################################################



# Functions ###################################################



# Let's Go !! #################################################


curl -sL https://bootstrap.saltstack.com -o install_salt.sh 2>&1 >/dev/null 
chmod 755 install_salt.sh
sudo sh install_salt.sh -P -M 2>&1 >/dev/null
sudo mkdir -p /home/vagrant/{salt,pillar}
sudo chown vagrant:vagrant  /home/vagrant/salt /home/vagrant/pillar

echo "
file_roots:
  base:
    - /home/vagrant/salt
    - /home/vagrant/pillar
" >> /etc/salt/master

echo "
master: 127.0.0.1
" >/etc/salt/minion

sudo systemctl restart salt-master
sudo systemctl restart salt-minion
sleep 10
sudo salt-key -A -y
sleep 5
sudo salt '*' test.ping

